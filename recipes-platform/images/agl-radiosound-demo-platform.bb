DESCRIPTION = "AGL Radio Sound Demo Platform image currently contains a thing."

require agl-radiosound-demo-platform.inc

LICENSE = "MIT"

IMAGE_FEATURES_append = " \
    "

# add packages for demo platform (include demo apps) here
IMAGE_INSTALL_append = " \
    packagegroup-agl-radiosound-demo-platform \
    "

