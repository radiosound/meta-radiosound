SUMMARY = "The software for demo platform of AGL Radio Sound profile"
DESCRIPTION = "A set of packages belong to AGL Radio Sound Demo Platform"

LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
    packagegroup-agl-radiosound-demo-platform \
    "

ALLOW_EMPTY_${PN} = "1"

RDEPENDS_${PN} += "\
    packagegroup-agl-image-ivi \
    packagegroup-agl-profile-graphical-qt5 \
    "

# fonts
TTF_FONTS = " \
    ttf-bitstream-vera \
    ttf-dejavu-sans \
    ttf-dejavu-sans-mono \
    ttf-dejavu-serif \
    source-han-sans-cn-fonts \
    source-han-sans-jp-fonts \
    source-han-sans-tw-fonts \
    "

AGL_APPS = " \
    ntouch-mediaplayer \
    "

AGL_APIS = " \
    agl-service-windowmanager \
    agl-service-can-low-level \
    agl-service-bluetooth \
    agl-service-mediaplayer \
    agl-service-signal-composer \
    "

RDEPENDS_${PN}_append = " \
    wayland-ivi-extension \
    radiosound-windowmanager-conf \
    hmi-debug \
    can-utils \
    libva-utils \
    linux-firmware-ralink \
    connman \
    connman-client \
    connman-tools \
    iproute2 \
    dhcp-client \
    qtquickcontrols2-agl \
    qtquickcontrols2-agl-style \
    ${TTF_FONTS} \
    ${AGL_APPS} \
    ${AGL_APIS} \
    udisks \
    evtest \
"

