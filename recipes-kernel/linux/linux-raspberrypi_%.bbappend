FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

#require recipes-kernel/linux/linux-agl.inc
#require recipes-kernel/linux/linux-agl-4.9.inc

SRC_URI_append = "\
    file://0001-Input-powermate-change-events-to-look-like-a-mouse-wheel.patch \
"

#TODO: are these supposed to go somewhere else?

KERNEL_DEVICETREE_raspberrypi3_append = "\ 
    overlays/rotary-encoder.dtbo \
    overlays/gpio-key.dtbo \
"

