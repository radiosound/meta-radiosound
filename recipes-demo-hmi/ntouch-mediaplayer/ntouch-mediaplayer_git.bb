SUMMARY     = "Radio Sound AGL Demonstration"
DESCRIPTION = "AGL HMI Application for demonstrating something"
HOMEPAGE    = "https://radiosound.com"
SECTION     = "apps"

LICENSE     = "Apache-2.0 & BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ae6497158920d9524cf208c09cc4c984"
# \
#                    file://app/cluster-gauges.qml;beginline=9;endline=48;md5=54187d50b29429abee6095fe8b7c1a78"

# SRC_URI = "gitsm://gitlab.apps.radiosound.com/engineering/agl/ntouch-mediaplayer.git;branch=${AGL_BRANCH};protocol=https"
SRC_URI = "gitsm://gitlab.apps.radiosound.com/engineering/agl/ntouch-mediaplayer.git;branch=1-move-playlist-to-its-own-page-make-it-bigger-and-easier-to-use;protocol=https"
#SRCREV  = "${AGL_APP_REVISION}"
SRCREV = "1-move-playlist-to-its-own-page-make-it-bigger-and-easier-to-use"

PV = "1.0+git${SRCPV}"
S  = "${WORKDIR}/git"

# build-time dependencies
DEPENDS += "qtquickcontrols2 qtwebsockets qlibwindowmanager libqtappfw qtsvg qtgamepad qtmultimedia libsdl2"

inherit qmake5 aglwgt

RDEPENDS_${PN} += " \
	qlibwindowmanager \
	libqtappfw \
        libsdl2 \
        qtgamepad \
        qtgamepad-qmlplugins \
        qtmultimedia \
        qtmultimedia-qmlplugins \
	qtquickcontrols \
	qtquickcontrols-qmlplugins \
	qtquickcontrols2 \
	qtquickcontrols2-qmlplugins \
	qtwebsockets \
	qtwebsockets-qmlplugins \
	agl-service-mediaplayer \
	agl-service-bluetooth \
	agl-service-signal-composer \
"


